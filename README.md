# bouncing-ball-test

When the user clicks the screen, some balls of random colours are instantiated. They are throwed on a random speed and angle, and bounce when they reach the bottom of the screen. Some attributes are editable by the end user, like gravity or size of the ball.

![picture](sample.PNG =427x340)

  * Technologies:
    * [TypeScript](https://www.typescriptlang.org/)
    * [Webpack](https://webpack.js.org/)
    * [Eslint](https://eslint.org/)
    * [Babel](https://babeljs.io/)
  * Webpack configuration is based on [JS-StarterKit](https://github.com/metalshan/js-starterkit)

## Build Setup
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
