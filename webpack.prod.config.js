const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const devConfig = require("./webpack.config.js");

const prodConfig = devConfig;

prodConfig.output = {
  path: path.resolve(__dirname, "dist"),
  filename: "index.js"
};
prodConfig.plugins.push(
  new CopyWebpackPlugin([
    {
      from: "index.html",
      to: path.resolve(__dirname, "dist/index.html"),
      transform: content =>
        content.toString().replace("src/style.css", "style.css")
    },
    { from: "src/style.css", to: path.resolve(__dirname, "dist/style.css") }
  ])
);

module.exports = prodConfig;
