const colors: string[] = [
  "#530127",
  "#db5f4d",
  "#e6b700",
  "#ece6dd",
  "#FFF0F5",
  "#F5DEB3",
  "#2E8B57",
  "#DC143C",
  "#BDB76B",
  "#FF6347",
  "#6495ED",
  "#FF8C00",
  "#E9967A",
  "#C71585",
  "#E0FFFF",
  "#008B8B",
  "#98FB98",
  "#EE82EE",
  "#F4A460",
  "#6A5ACD",
  "#BC8F8F",
  "#FFF0F5",
  "#98FB98",
  "#FF8C00"
];

export default colors;
