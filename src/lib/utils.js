export function resetStyles(element) {
  for (let styleName in element.style) {
    const descriptor = Object.getOwnPropertyDescriptor(
      element.style,
      styleName
    );
    if (descriptor && descriptor.writable) {
      // console.log("name = ", styleName);
      element.style[styleName] = "";
    }
  }
}
