import { PhysicsObjectOptions } from "./PhysicsObjectOptions";
import { Pool } from "./Pool";

/**
  An object with physics characteristics that is updated by Physics class.
*/
export abstract class PhysicsObject {
  //////////////////////////////////////////////////////////////////////////////
  // Constants                                                                //
  //////////////////////////////////////////////////////////////////////////////
  public static readonly DEFAULT_LIFE_TIME = 5 * 1000;
  public static readonly DEFAULT_SIZE = 10;
  private static readonly DEFAULT_TAG = "div";
  private static readonly DEFAULT_GRAVITY_MULTIPLIER = 1;
  private static readonly DEFAULT_SPEED = 0;
  private static readonly DEFAULT_COLOR = "black";
  private static readonly DEFAULT_CLASS = "physics-object";

  //////////////////////////////////////////////////////////////////////////////
  // Variables                                                                //
  //////////////////////////////////////////////////////////////////////////////
  public id: number;
  protected el: HTMLElement;
  protected tag: string;
  protected pool: Pool;
  protected maxLifeTime: number;
  protected lifeTime: number;
  private destroying: boolean;
  private destroyed: boolean;
  public gravityMultiplier: number;
  public x: number;
  public y: number;
  public xSpeed: number;
  public ySpeed: number;
  public realXSpeed: number;
  public realYSpeed: number;
  public height: number;
  public width: number;
  public color: string;
  protected onAir: boolean;
  protected framesOnAir: number;
  protected previousFramesOnAir: number;
  protected onScreen: boolean;

  /**
    Initialization of the PhysicsObject
  */
  protected init(options: PhysicsObjectOptions) {
    this.maxLifeTime =
      options.maxLifeTime ||
      PhysicsObject.DEFAULT_LIFE_TIME + Math.random() * 3000;
    this.tag = options.tag || PhysicsObject.DEFAULT_TAG;
    this.gravityMultiplier =
      options.gravityMultiplier || PhysicsObject.DEFAULT_GRAVITY_MULTIPLIER;
    this.x = options.x;
    this.y = options.y;
    this.xSpeed = options.xSpeed || PhysicsObject.DEFAULT_SPEED;
    this.ySpeed = options.ySpeed || PhysicsObject.DEFAULT_SPEED;
    this.realXSpeed = this.xSpeed;
    this.realYSpeed = this.ySpeed;
    this.color = options.color || PhysicsObject.DEFAULT_COLOR;
    this.height = options.height || PhysicsObject.DEFAULT_SIZE;
    this.width = options.width || PhysicsObject.DEFAULT_SIZE;

    this.lifeTime = 0;
    this.destroying = false;
    this.destroyed = false;
    this.onAir = true;
    this.framesOnAir = 0;
    this.previousFramesOnAir = 0;
    this.onScreen = true;
  }

  constructor(options: PhysicsObjectOptions) {
    this.init(options);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Lifecycle                                                                //
  //////////////////////////////////////////////////////////////////////////////
  /**
    Starts the physic logic of the Missile
  */
  public start() {
    this.el = this.pool.requestHTMLElement(this.tag);
    this.el.classList.add(PhysicsObject.DEFAULT_CLASS);
  }

  /**
    Physic update. It's called by Physics class.
    @param time Time from previous update.
    @param deltaTime Porcentual deviation from the selected refresh date on Physics class.
    @param options Options of the Physic Object.
  */
  public update(
    time: number,
    deltaTime: number,
    options: PhysicsObjectOptions = null
  ) {
    this.checkLifeTime(time);
    if (!this.destroying) {
      this.checkOnScreen();
      if (this.onScreen) {
        if (options !== null) {
          this.init(options);
        }
        if (this.onAir) {
          this.framesOnAir += deltaTime;
          this.x += this.xSpeed * deltaTime;
          this.y += this.ySpeed * deltaTime;
        }
      } else {
        this.destroy();
      }
    } else {
      this.destroy();
    }
  }

  /**
    Style update of the DOM element
  */
  public graphicUpdate() {
    if (!this.destroying && this.onScreen) {
      this.el.style.left = `${Math.ceil(this.x)}px`;
      this.el.style.top = `${Math.ceil(this.y)}px`;
      this.el.style.height = `${Math.ceil(this.height)}px`;
      this.el.style.width = `${Math.ceil(this.width)}px`;
    }
  }

  /**
    When called, the object will no longer be updated by Physics class.
  */
  public destroy() {
    this.destroyed = true;
    this.pool.giveBackHTMLElement(this.el);
  }

  /**
    Called when the PhysicsObject hits the floor.
  */
  public hitFloor() {
    this.onAir = false;
    this.previousFramesOnAir = this.framesOnAir;
    this.framesOnAir = 0;
  }

  /**
    Called when the PhysicsObject leaves the floor.
  */
  public tookOffFloor() {
    this.onAir = true;
  }

  /**
    Checks the life time of the object in order to destroy it.
  */
  private checkLifeTime(time: number) {
    this.lifeTime += time;
    if (this.lifeTime >= this.maxLifeTime) {
      this.destroying = true;
    }
  }

  /**
    Checks if the object is on the screen.
  */
  private checkOnScreen() {
    this.onScreen =
      this.x <= window.outerWidth + this.width &&
      this.x >= -this.width &&
      this.y <= window.outerHeight + this.height;
  }

  //////////////////////////////////////////////////////////////////////////////
  // Setters and getters                                                      //
  //////////////////////////////////////////////////////////////////////////////
  public setPool(pool: Pool) {
    this.pool = pool;
  }

  public isBeingDestroyed(): boolean {
    return this.destroying;
  }

  public hasBeenDestroyed(): boolean {
    return this.destroyed;
  }

  public getEl(): HTMLElement {
    return this.el;
  }

  public setOnAir(onAir: boolean) {
    this.onAir = onAir;
  }

  public isOnAir(): boolean {
    return this.onAir;
  }

  public getFramesOnAir(): number {
    return this.framesOnAir;
  }
}
