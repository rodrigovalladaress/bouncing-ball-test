import { PhysicsObjectOptions } from "./PhysicsObjectOptions";
import { FloorBehaviour } from "./Missile";

export interface MissileOptions extends PhysicsObjectOptions {
  initialAngle: number;
  color: string;
  trailSize?: number;
  floorBehaviour?: FloorBehaviour;
  restitutionCoef?: number;
  trailMaxLifeTime?: number;
}
