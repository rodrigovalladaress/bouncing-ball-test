import { Physics } from "./Physics";

export interface PhysicsObjectOptions {
  maxLifeTime?: number;
  tag?: string;
  gravityMultiplier?: number;
  x: number;
  y: number;
  xSpeed?: number;
  ySpeed?: number;
  height?: number;
  width?: number;
  color?: string;
}
