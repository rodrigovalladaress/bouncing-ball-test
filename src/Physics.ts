import { PhysicsObject } from "./PhysicsObject";
import { PhysicsObjectOptions } from "./PhysicsObjectOptions";
import { Pool } from "./Pool";

/**
  Stores PhysicsObjects, updates them  and applies them gravity.
*/
export class Physics {
  //////////////////////////////////////////////////////////////////////////////
  // Constants                                                                //
  //////////////////////////////////////////////////////////////////////////////
  public static readonly MAX_NUMBER_OBJECTS: number = 100;
  public static readonly EARTH_GRAVITY: number = 9.81;
  public static readonly DEFAULT_GRAVITY: number = 0.02;
  public static readonly REFRESH_RATE: number = 10;

  //////////////////////////////////////////////////////////////////////////////
  // Variables                                                                //
  //////////////////////////////////////////////////////////////////////////////
  protected pool: Pool;
  private numberOfObjects: number;
  private root: HTMLElement;
  private gravity: number;
  private objects: PhysicsObject[];
  private interval: number;
  private lastUpdate: Date;
  private deltaTime: number;
  private currentId: number;

  //////////////////////////////////////////////////////////////////////////////
  // Lifecycle                                                                //
  //////////////////////////////////////////////////////////////////////////////
  public constructor(
    root: HTMLElement,
    gravity: number = Physics.DEFAULT_GRAVITY
  ) {
    this.pool = new Pool();
    this.numberOfObjects = 0;
    this.root = root;
    this.gravity = gravity;
    this.objects = [];
    this.currentId = 0;
    this.start();
  }

  public start() {
    this.interval = setInterval(this.update.bind(this), Physics.REFRESH_RATE);
  }

  private update() {
    const now: Date = new Date();
    const maxY: number = window.innerHeight;
    let objectPosToDestroy: number[] = [];

    this.deltaTime = this.lastUpdate
      ? (now.getTime() - this.lastUpdate.getTime()) / Physics.REFRESH_RATE
      : 1;
    this.lastUpdate = now;

    for (let i = 0; i < this.objects.length; i++) {
      const o = this.objects[i];
      if (!o.hasBeenDestroyed()) {
        if (!o.isBeingDestroyed()) {
          this.applyGravity(o, maxY);
        }
        o.update(Physics.REFRESH_RATE * this.deltaTime, this.deltaTime);
        o.graphicUpdate();
      } else {
        // Collects the objects that has been destroyed to remove them from the
        // objects that are been updated.
        objectPosToDestroy.push(i);
      }
    }

    // Pop destroyed objects from the objects array.
    if (objectPosToDestroy.length > 0) {
      let lastObjectPos = this.objects.length - 1;
      // Sort the array positions stored on objectPosToDestroy to avoid errors
      // when swapping elements of the array later.
      objectPosToDestroy = objectPosToDestroy.sort((a, b) => b - a);

      // Put objects thar are going to be destroyed at the end of the array.
      for (let i of objectPosToDestroy) {
        const aux = this.objects[lastObjectPos];
        this.objects[lastObjectPos] = this.objects[i];
        this.objects[i] = aux;
        lastObjectPos--;
      }
      for (let i = 0; i < objectPosToDestroy.length; i++) {
        this.objects.pop();
        this.numberOfObjects--;
      }
    }
  }

  public stop() {
    clearInterval(this.interval);
  }

  //////////////////////////////////////////////////////////////////////////////
  // Methods                                                                  //
  //////////////////////////////////////////////////////////////////////////////
  /**
    Applies gravity to a PhysicsObject.
  */
  private applyGravity(o: PhysicsObject, maxY: number) {
    const floorLevel = maxY - o.height;
    if (o.y + o.height < maxY) {
      if (!o.isOnAir()) {
        o.tookOffFloor();
      }
      o.realYSpeed =
        o.ySpeed +
        this.gravity *
          o.gravityMultiplier *
          this.deltaTime *
          o.getFramesOnAir();
      o.y += o.realYSpeed;
    } else {
      // Check if object already hit floor.
      if (o.isOnAir()) {
        o.hitFloor();
      }
      o.y = floorLevel;
    }
  }

  /**
    Adds a new PhysicsObject to the array of objects and inits it.
  */
  public add(o: PhysicsObject) {
    if (this.numberOfObjects < Physics.MAX_NUMBER_OBJECTS) {
      o.id = this.currentId;
      o.setPool(this.pool);
      o.start();
      this.currentId++;
      this.objects.push(o);
      this.root.appendChild(o.getEl());
      this.numberOfObjects++;
    }
  }
}
