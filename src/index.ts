import { Physics } from "./Physics";
import { PhysicsObject } from "./PhysicsObject";
import { Missile, FloorBehaviour } from "./Missile";
import { MissileOptions } from "./MissileOptions";
import colors from "./lib/randomColors";

const PI = 3.14;
const MIN_ANGLE = PI;
const MAX_ANGLE = 2 * PI;
const DEFAULT_MISSILES_PER_CLICK = 1;

const app: HTMLElement = document.getElementById("app");
const container: HTMLElement = document.getElementById("container");
const stars: HTMLElement = document.getElementById("stars");
const formContainer: HTMLElement = <HTMLElement>document.getElementsByClassName(
  "form-container"
)[0];
const form: HTMLElement = document.getElementById("form");
const settings: HTMLElement = document.getElementById("settings");
const physics = new Physics(container);
const numColors = colors.length;
let settingsOn = false;
// Form inputs
let formInputs: { [id: string]: HTMLInputElement };
// Form values
let missilesPerClick = DEFAULT_MISSILES_PER_CLICK;
let gravityMultiplier = 1;
let missileDuration = PhysicsObject.DEFAULT_LIFE_TIME / 1000;
let trailDuration = Missile.DEFAULT_TRAIL_MAX_LIFE_TIME / 1000;
let missileSize = PhysicsObject.DEFAULT_SIZE;
let trailSize = Missile.DEFAULT_TRAIL_SIZE;

function init() {
  printStars();

  formInputs = {
    missilesPerClick: <HTMLInputElement>document.getElementById(
      "missiles-per-click"
    ),
    gravity: <HTMLInputElement>document.getElementById("gravity"),
    missileDuration: <HTMLInputElement>document.getElementById(
      "missile-duration"
    ),
    trailDuration: <HTMLInputElement>document.getElementById("trail-duration"),
    missileSize: <HTMLInputElement>document.getElementById("missile-size"),
    trailSize: <HTMLInputElement>document.getElementById("trail-size")
  };

  // Add listeners
  container.onclick = ({ clientX, clientY }) => {
    addRandomMissile(clientX, clientY);
  };
  settings.onclick = () => {
    checkFormValues();
    if (settingsOn) {
      formContainer.classList.add("hide");
    } else {
      formContainer.classList.remove("hide");
    }
    settingsOn = !settingsOn;
  };

  // Initialize form values
  formInputs.missilesPerClick.value = `${missilesPerClick}`;
  formInputs.gravity.value = `${gravityMultiplier}`;
  formInputs.missileDuration.value = `${missileDuration}`;
  formInputs.trailDuration.value = `${trailDuration}`;
  formInputs.missileSize.value = `${missileSize}`;
  formInputs.trailSize.value = `${trailSize}`;
}

function printStars() {
  for (let i = 0; i < 1000; i++) {
    const star = document.createElement("div");
    star.classList.add("star");
    star.style.left = `${Math.floor(Math.random() * window.outerWidth)}px`;
    star.style.top = `${Math.floor(Math.random() * window.outerHeight)}px`;
    stars.appendChild(star);
  }
}

function checkFormValues() {
  try {
    missilesPerClick = parseFloat(formInputs.missilesPerClick.value);
  } catch (err) {
    console.error(err);
    formInputs.missilesPerClick.value = `${missilesPerClick}`;
  }
  try {
    gravityMultiplier = parseFloat(formInputs.gravity.value);
  } catch (err) {
    console.error(err);
    formInputs.gravity.value = `${gravityMultiplier}`;
  }
  try {
    missileDuration = parseFloat(formInputs.missileDuration.value);
  } catch (err) {
    console.error(err);
    formInputs.missileDuration.value = `${missileDuration}`;
  }
  try {
    trailDuration = parseFloat(formInputs.trailDuration.value);
  } catch (err) {
    console.error(err);
    formInputs.trailDuration.value = `${trailDuration}`;
  }
  try {
    missileSize = parseFloat(formInputs.missileSize.value);
  } catch (err) {
    console.error(err);
    formInputs.missileSize.value = `${missileSize}`;
  }
  try {
    trailSize = parseFloat(formInputs.trailSize.value);
  } catch (err) {
    console.error(err);
    formInputs.trailSize.value = `${trailSize}`;
  }
}

function addRandomMissile(x: number, y: number) {
  checkFormValues();
  for (let i = 0; i < missilesPerClick; i++) {
    const missile: Missile = new Missile({
      initialAngle: MIN_ANGLE + MAX_ANGLE * Math.random(),
      color: colors[Math.floor(Math.random() * numColors)],
      x,
      y,
      xSpeed: 2 * Math.random() - 1,
      ySpeed: 2 * Math.random(),
      gravityMultiplier,
      floorBehaviour: FloorBehaviour.bouncy,
      maxLifeTime: missileDuration * 1000,
      trailMaxLifeTime: trailDuration * 1000,
      height: missileSize,
      width: missileSize,
      trailSize
    });
    physics.add(missile);
  }
}

init();
