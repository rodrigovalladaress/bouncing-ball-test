import { resetStyles } from "./lib/utils";

/**
  Pool of HTMLElements. Store HTMLElements that can be requested, so it controls
  the number of DOM elements that are been created to avoid too much overhead.
  The elements are stored by tag.
*/
export class Pool {
  private root: HTMLElement;
  private pool: { [id: string]: HTMLElement[] } = {};

  constructor(sizeOptions: { [id: string]: number } = { div: 1000 }) {
    this.root = document.createElement("div");
    this.root.id = "pool";
    document.body.appendChild(this.root);

    const keys = Object.keys(sizeOptions);

    for (const tag of keys) {
      const tagLower = tag.toLowerCase();
      const size = sizeOptions[tagLower];
      this.pool[tagLower] = [];
      for (let i = 0; i < size; i++) {
        this.pool[tagLower].push(this.createElement(tagLower));
      }
    }
  }

  private createElement(tag: string): HTMLElement {
    const element = document.createElement(tag.toLowerCase());
    this.root.appendChild(element);
    return element;
  }

  public requestHTMLElement(tag: string): HTMLElement {
    let element: HTMLElement = null;
    const tagLower = tag.toLowerCase();
    if (this.pool[tagLower] && this.pool[tagLower].length > 0) {
      element = this.pool[tagLower].pop();
      element.className = "";
      resetStyles(element);
    } else {
      element = this.createElement(tagLower);
    }
    return element;
  }

  public giveBackHTMLElement(element: HTMLElement) {
    const tag = element.tagName.toLowerCase();
    this.root.appendChild(element);

    if (!this.pool[tag]) {
      this.pool[tag] = [];
    }
    this.pool[tag].push(element);
  }
}
