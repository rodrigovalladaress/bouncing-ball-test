import { PhysicsObject } from "./PhysicsObject";
import { MissileOptions } from "./MissileOptions";

export enum FloorBehaviour {
  sticky,
  bouncy
}

/**
  Missile that starts moving towards a selected angle and  with a selected
  speed (x, y) and bounces on the floor
*/
export class Missile extends PhysicsObject {
  //////////////////////////////////////////////////////////////////////////////
  // Constants                                                                //
  //////////////////////////////////////////////////////////////////////////////
  public static readonly MISSILE_CLASS = "missile";
  public static readonly TRAIL_CLASS = "trail";
  public static readonly HIDE_CLASS = "fade-out";
  public static readonly TRAIL_TAG = "div";
  public static readonly DEFAULT_FLOOR_BEHAVIOUR = FloorBehaviour.sticky;
  public static readonly DEFAULT_TRAIL_MAX_LIFE_TIME = 1 * 1000;
  private static readonly DEFAULT_RESTITUTION_COEF = 0.8;
  public static readonly DEFAULT_TRAIL_SIZE = 1;

  //////////////////////////////////////////////////////////////////////////////
  // Variables                                                                //
  //////////////////////////////////////////////////////////////////////////////
  private initialAngle: number;
  private restitutionCoef: number;
  private missile: HTMLElement;
  private trail: HTMLElement[];
  private lastTrail: HTMLElement;
  private numTrailDestroyed: number;
  private trailMaxLifeTime: number;
  private floorBehaviour: FloorBehaviour;
  private trailWidth: number;
  private trailHeight: number;
  private trailColor: string;
  private timeToHideMissile: number;

  //////////////////////////////////////////////////////////////////////////////
  // Lifecycle                                                                //
  //////////////////////////////////////////////////////////////////////////////
  /**
    Initialization of the Missile
  */
  init(options: MissileOptions) {
    super.init(options);
    this.initialAngle = options.initialAngle;
    // Calculates initial speed by the initial angle.
    // https://en.wikipedia.org/wiki/Projectile_motion
    this.xSpeed = this.xSpeed * Math.cos(this.initialAngle);
    this.ySpeed = this.ySpeed * Math.sin(this.initialAngle);
    this.floorBehaviour =
      options.floorBehaviour || Missile.DEFAULT_FLOOR_BEHAVIOUR;

    this.trail = [];
    this.numTrailDestroyed = 0;
    this.trailWidth = options.trailSize || Missile.DEFAULT_TRAIL_SIZE;
    this.trailHeight = options.trailSize || Missile.DEFAULT_TRAIL_SIZE;
    this.trailColor = this.color;
    this.trailMaxLifeTime =
      options.trailMaxLifeTime || Missile.DEFAULT_TRAIL_MAX_LIFE_TIME;
    this.timeToHideMissile = this.maxLifeTime * 0.7;

    this.restitutionCoef =
      options.restitutionCoef || Missile.DEFAULT_RESTITUTION_COEF;
  }

  constructor(options: MissileOptions) {
    super(options);
  }

  start() {
    super.start();
    this.el.classList.add(Missile.MISSILE_CLASS);
    this.el.style.backgroundColor = this.color;
    this.el.style.borderColor = this.color;
  }

  public update(
    time: number,
    deltaTime: number,
    options: MissileOptions = null
  ) {
    super.update(time, deltaTime, options);
    // Add a trail
    if (!this.isBeingDestroyed() && this.onScreen && this.onAir) {
      this.lastTrail = this.pool.requestHTMLElement(Missile.TRAIL_TAG);
      this.lastTrail.classList.add(Missile.TRAIL_CLASS);
      this.trail.push(this.lastTrail);
      this.el.parentElement.appendChild(this.lastTrail);
    }
    if (!this.hasBeenDestroyed()) {
      const numTrails = this.trail.length;
      // Hide trail
      if (
        this.lifeTime >= this.trailMaxLifeTime &&
        this.numTrailDestroyed < numTrails
      ) {
        this.trail[this.numTrailDestroyed].classList.add(Missile.HIDE_CLASS);
        this.numTrailDestroyed++;
        if (this.numTrailDestroyed === numTrails) {
          // Wait for the last trail to disappear to destroy the object.
          setTimeout(() => {
            this.giveBackTrails();
            super.destroy();
          }, this.trailMaxLifeTime);
        }
      }
      // Hide missile
      if (this.lifeTime >= this.timeToHideMissile) {
        this.el.classList.add(Missile.HIDE_CLASS);
      }
    }
  }

  public graphicUpdate() {
    super.graphicUpdate();
    if (
      !this.isBeingDestroyed() &&
      this.onScreen &&
      this.onAir &&
      this.lastTrail
    ) {
      this.lastTrail.style.left = `${Math.ceil(this.x)}px`;
      this.lastTrail.style.top = `${Math.ceil(this.y)}px`;
      this.lastTrail.style.height = `${Math.ceil(this.trailHeight)}px`;
      this.lastTrail.style.width = `${Math.ceil(this.trailWidth)}px`;
      this.lastTrail.style.backgroundColor = this.color;
      this.lastTrail.style.borderColor = this.color;
    }
  }

  public hitFloor() {
    super.hitFloor();
    if (!this.isBeingDestroyed() && this.onScreen) {
      if (this.floorBehaviour === FloorBehaviour.sticky) {
        this.xSpeed = 0;
        this.ySpeed = 0;
      } else {
        // Simmulate the bounce of the missile by its restitution coefficient.
        this.ySpeed = -this.ySpeed * this.restitutionCoef;
        super.tookOffFloor();
      }
    }
  }

  public destroy() {
    // Avoid been destroyed until the last trail has disappeared. It is
    // destroyed on update().
  }

  /**
    Give back trails to the pool
  */
  private giveBackTrails() {
    const parent: HTMLElement = this.el.parentElement;
    for (const o of this.trail) {
      this.pool.giveBackHTMLElement(o);
    }
  }
}
